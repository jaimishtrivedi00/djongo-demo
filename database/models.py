from djongo import models
# from django.conf import settings

# Add the import for GridFSStorage
# from djongo.storage import GridFSStorage

# grid_fs_storage = GridFSStorage(collection='myfiles', base_url=''.join(
#     ['https://localhost:8000/media/', 'myfiles/']))


class Blog(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100)
    tagline = models.TextField()

    def __str__(self):
        return self.name


class Author(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=200)
    email = models.EmailField()
    # avatar = models.ImageField(upload_to='authors')
    avatar = models.ImageField(upload_to='authors/', blank=True, null=True)

    def __str__(self):
        return self.name


class Entry(models.Model):
    _id = models.ObjectIdField()
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    headline = models.CharField(max_length=255)
    body_text = models.TextField()
    pub_date = models.DateField()
    mod_date = models.DateField()
    authors = models.ManyToManyField(Author)
    n_comments = models.IntegerField()
    n_pingbacks = models.IntegerField()
    rating = models.IntegerField()
    # featured_image = models.ImageField(upload_to='entries')
    featured_image = models.ImageField(
        upload_to='entries/', blank=True, null=True)

    def __str__(self):
        return self.headline

    class Meta:
        verbose_name_plural = 'Entries'