from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from bson import ObjectId

from .serializers import AuthorSerializer, EntrySerializer, BlogSerializer
from .models import Author, Entry, Blog


class BlogViewSet(viewsets.ModelViewSet):
    permission_classes_by_action = {'create': [
        AllowAny], 'update': [AllowAny], 'destroy': [AllowAny]}

    queryset = Blog.objects.all().order_by('name')
    serializer_class = BlogSerializer

    def create(self, request):
        data = JSONParser().parse(request)
        blog_serializer = BlogSerializer(data=data)

        if blog_serializer.is_valid():
            blog_serializer.save()
            return JsonResponse(blog_serializer.data)

        return JsonResponse(blog_serializer.errors, status=400)

    def retrieve(self, request, id):
        try:
            blog = Blog.objects.get(_id=ObjectId(id))
        except Blog.DoesNotExist:
            return JsonResponse({'msg': 'Blog doesn\'t exist'}, status=404)

        blog_serializer = BlogSerializer(blog)
        return JsonResponse(blog_serializer.data)

    def update(self, request, id):
        data = JSONParser().parse(request)
        try:
            blog = Blog.objects.get(_id=ObjectId(id))
        except Blog.DoesNotExist:
            return JsonResponse({'msg': 'Blog doesn\'t exist'}, status=404)

        blog_serializer = BlogSerializer(blog, data=data, partial=True)

        if blog_serializer.is_valid():
            blog_serializer.save()
            return JsonResponse(blog_serializer.data)

        return JsonResponse(blog_serializer.errors, status=400)

    def destroy(self, request, id):
        # data = JSONParser().parse(request)
        try:
            blog = Blog.objects.get(_id=ObjectId(id))
            blog.delete()
            
        except Blog.DoesNotExist:
            return JsonResponse({'msg': 'Blog doesn\'t exist'}, status=404)

        return JsonResponse({'msg': 'Blog deleted'}, status=204)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


class AuthorViewSet(viewsets.ModelViewSet):
    permission_classes_by_action = {'create': [
        AllowAny], 'update': [AllowAny], 'destroy': [AllowAny]}

    queryset = Author.objects.all().order_by('name')
    serializer_class = AuthorSerializer

    def create(self, request):
        data = JSONParser().parse(request)
        author_serializer = AuthorSerializer(data=data)

        if author_serializer.is_valid():
            author_serializer.save()
            return JsonResponse(author_serializer.data)

        return JsonResponse(author_serializer.errors, status=400)

    def retrieve(self, request, id):
        try:
            author = Author.objects.get(_id=ObjectId(id))            
        except Author.DoesNotExist:
            return JsonResponse({'msg': 'Author doesn\'t exist'}, status=404)

        author_serializer = AuthorSerializer(author)
        return JsonResponse(author_serializer.data)
        # return JsonResponse({'name': author.name, 'email': author.email, 'avatar': str(author.avatar)})

    def update(self, request, id):
        data = JSONParser().parse(request)
        
        try:
            author = Author.objects.get(_id=ObjectId(id))            
        except Author.DoesNotExist:
            return JsonResponse({'msg': 'Author doesn\'t exist'}, status=404)

        author_serializer = AuthorSerializer(author, data=data, partial=True)

        if author_serializer.is_valid():
            author_serializer.save()
            return JsonResponse(author_serializer.data)

        return JsonResponse(author_serializer.errors, status=400)

    def destroy(self, request, id):
        # data = JSONParser().parse(request)
        try:
            author = Author.objects.get(_id=ObjectId(id))  
            author.delete()
                      
        except Author.DoesNotExist:
            return JsonResponse({'msg': 'Author doesn\'t exist'}, status=404)

        return JsonResponse({'msg': 'Author deleted'}, status=204)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


class EntryViewSet(viewsets.ModelViewSet):
    permission_classes_by_action = {'create': [
        AllowAny], 'update': [AllowAny], 'destroy': [AllowAny]}

    queryset = Entry.objects.all().order_by('headline')
    serializer_class = EntrySerializer

    def create(self, request):
        data = JSONParser().parse(request)
        entry_serializer = EntrySerializer(data=data)

        if entry_serializer.is_valid():
            entry_serializer.save()
            return JsonResponse(entry_serializer.data)

        return JsonResponse(entry_serializer.errors, status=400)

    def retrieve(self, request, id):
        try:
            entry = Entry.objects.get(_id=ObjectId(id))            
        except Entry.DoesNotExist:
            return JsonResponse({'msg': 'Entry doesn\'t exist'}, status=404)

        entry_serializer = EntrySerializer(entry)
        return JsonResponse(entry_serializer.data)

    def update(self, request, id):
        data = JSONParser().parse(request)
        try:
            entry = Entry.objects.get(_id=ObjectId(id))            
        except Entry.DoesNotExist:
            return JsonResponse({'msg': 'Entry doesn\'t exist'}, status=404)

        entry_serializer = EntrySerializer(entry, data=data, partial=True)

        if entry_serializer.is_valid():
            entry_serializer.save()
            return JsonResponse(entry_serializer.data)

        return JsonResponse(entry_serializer.errors, status=400)

    def destroy(self, request, id):
        # data = JSONParser().parse(request)
        try:
            entry = Entry.objects.get(_id=ObjectId(id)) 
            entry.delete()
                       
        except Entry.DoesNotExist:
            return JsonResponse({'msg': 'Entry doesn\'t exist'}, status=404)

        return JsonResponse({'msg': 'Entry deleted'}, status=204)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]
