from rest_framework import routers
from django.urls import path, include
from . import views

router = routers.DefaultRouter()
router.register('authors', views.AuthorViewSet)
router.register('entries', views.EntryViewSet)
router.register('blogs', views.BlogViewSet)

author_methods = views.AuthorViewSet.as_view({
    'put': 'update',
    'delete': 'destroy',
    'get': 'retrieve',
})
entry_methods = views.EntryViewSet.as_view({
    'put': 'update',
    'delete': 'destroy',
    'get': 'retrieve',
})
blog_methods = views.BlogViewSet.as_view({
    'put': 'update',
    'delete': 'destroy',
    'get': 'retrieve',
})

urlpatterns = [
    path('authors/<str:id>/', author_methods, name='author_methods'),
    path('entries/<str:id>/', entry_methods, name='entry_methods'),
    path('blogs/<str:id>/', blog_methods, name='blog_methods'),
    path('', include(router.urls))
]
