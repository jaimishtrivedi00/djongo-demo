from django.db import transaction
from rest_framework import serializers
from .models import Author, Entry, Blog


class BlogSerializer(serializers.HyperlinkedModelSerializer):
    @transaction.atomic()
    def create(self, validated_data):
      return Blog.objects.create(**validated_data)

    @transaction.atomic()
    def update(self, instance, validated_data):
      for attr, value in validated_data.items():
          setattr(instance, attr, value)
      instance.save()
      return instance

    class Meta:
        model = Blog
        fields = ('_id', 'name', 'tagline')


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    avatar = serializers.ImageField(
        max_length=None, allow_empty_file=False, allow_null=True, required=False)

    @transaction.atomic()
    def create(self, validated_data):
      return Author.objects.create(**validated_data)

    @transaction.atomic()
    def update(self, instance, validated_data):
      for attr, value in validated_data.items():
          setattr(instance, attr, value)
      instance.save()
      return instance

    class Meta:
        model = Author
        fields = ('_id', 'name', 'email', 'avatar')


class EntrySerializer(serializers.HyperlinkedModelSerializer):
    featured_image = serializers.ImageField(
        max_length=None, allow_empty_file=False, allow_null=True, required=False)
    blog = BlogSerializer()
    authors = AuthorSerializer(many=True)

    @transaction.atomic()
    def create(self, validated_data):
      blog_data = validated_data.pop('blog', None)
      blog, blog_created = Blog.objects.get_or_create(**blog_data)
      if not blog_created:
        blog.save()
      
      authors_data = validated_data.pop('authors', None)
      
      entry = Entry.objects.create(**validated_data, blog=blog)
      for author_data in authors_data:
        author, author_created = Author.objects.get_or_create(**author_data)
        entry.authors.add(author)
        if not author_created:
          author.save()
      
      return entry

    @transaction.atomic()
    def update(self, instance, validated_data):
      try:
        blog = instance.blog
        blog_data = validated_data.pop('blog', None)
        for attr, value in blog_data.items():
          setattr(blog, attr, value)
        blog.save()
      except:
        pass
      
      try:
        authors = instance.authors
        authors_data = validated_data.pop('authors', None)
        for idx, author_data in enumerate(authors_data):
          for attr, value in author_data.items():
            setattr(authors[idx], attr, value)
          authors[idx].save()
      except:
        pass
        
      for attr, value in validated_data.items():
        setattr(instance, attr, value)
        
      instance.save()
      
      return instance

    class Meta:
        model = Entry
        fields = ('_id', 'blog', 'headline', 'body_text', 'pub_date', 'mod_date',
                  'authors', 'n_comments', 'n_pingbacks', 'rating', 'featured_image')
